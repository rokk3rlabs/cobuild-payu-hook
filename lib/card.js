/**
 * Module dependencies.
 */

var request               = require('request');

/**
 * Expose
 */

module.exports = function(credentials){
  var apiLogin              = credentials.apiLogin;
  var apiKey                = credentials.apiKey;
  auth                      = "Basic "+ new Buffer(apiLogin + ":" + apiKey).toString("base64");

  var service = {};

  /**
   *
   * Create customer on PayU 
   *
   * @param  {Object} params All customer data (required)
   * 
   */


  service.create = function (card, payer){
    return new Promise((resolve, reject) => {
      var requireData = [
        card.token, 
        payer.id_payu,
        credentials.apiLogin,
        credentials.apiKey,
        credentials.payuUrlPayment
      ];

      requireData.forEach(function(data){
        if(!data){
          reject({'require': 'Card data is require'});
        }
      });

      var data = {
        "language": "es",
        "command": "REMOVE_TOKEN",
        "merchant": {
          "apiKey": credentials.apiKey,
          "apiLogin": credentials.apiLogin
        },
        "removeCreditCardToken":{
          "payerId": payer.id_payu,
          "creditCardTokenId": card.token
        }
      };

      request({
        url: credentials.payuUrlPayment,
        method: 'POST',
        headers: {
          Accept: 'application/json', 
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization': auth
        },
        strictSSL: false,
        body: data,
        json: true
      }, 
      function (err, response, body){
        resolve(response.body)
      })

    })
  }


  service.delete = function(card, payer){
    console.log(card)
    return new Promise((resolve, reject) => {

      var requireData = [
        card,
        payer,
        credentials.apiLogin, 
        credentials.apiKey, 
        credentials.payuUrlPayment,

      ];

      requireData.forEach(function(data){
        if(!data){
          reject({'require':'Card data is require.'});
        }
      });

      var data = {
        "language": "es",
        "command": "REMOVE_TOKEN",
        "merchant": {
          "apiKey": credentials.apiKey,
          "apiLogin": credentials.apiLogin
        },
        "removeCreditCardToken": {
          "payerId": payer,
          "creditCardTokenId": card
        }
      };

      request({
        url: credentials.payuUrlPayment,
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization' : auth
        },
        strictSSL: false,
        body: data,
        json: true
      }, 

      function (err, response, body) { 
        resolve(response.body);
      });
    });
  };


  service.find = function(card, payer){
    return new Promise((resolve, reject) => {

      var requireData = [
        card.token,
        payer.id_payu,
        credentials.apiLogin, 
        credentials.apiKey, 
        credentials.payuUrlPayment,

      ];

      requireData.forEach(function(data){
        if(!data){
          reject({'require':'Card data is require.'});
        }
      });

      var data = {
        "language": "es",
        "command": "GET_TOKENS",
        "merchant": {
          "apiKey": credentials.apiKey,
          "apiLogin": credentials.apiLogin
        },
        "creditCardTokenInformation": {
          "payerId": payer.id_payu,
          "creditCardTokenId": card.token
        }
      };
      request({
        url: credentials.payuUrlPayment,
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization' : auth
        },
        strictSSL: false,
        body: data,
        json: true
      }, 

      function (err, response, body) {      
        resolve(response.body);
      });
    });
  };


  return service;
};