/**
 * Module dependencies.
 */


var request               = require('request');
var crypto                = require('crypto');


/**
 * Expose
 */

module.exports = function (credentials) {

  var apiLogin              = credentials.apiLogin;
  var apiKey                = credentials.apiKey;
  auth                      = "Basic "+ new Buffer(apiLogin + ":" + apiKey).toString("base64");

  var service = {}; 

  /**
   *
   * Create get banks list of PSE PayU 
   *
   *
   */
   service.getBanks = function(){
    return new Promise((resolve, reject) => {
      var requireData = [
        apiLogin,
        apiKey
      ];

      requireData.forEach(function(data){
          if(!data){
              reject({'require':'Payment data is require.'});
          }
      });

      var data = {
       "language": "es",
       "command": "GET_BANKS_LIST",
       "merchant": {
          "apiLogin": apiLogin,
          "apiKey": apiKey
       },
       "test": false,
       "bankListInformation": {
          "paymentMethod": "PSE",
          "paymentCountry": "CO"
       }
    }
    request({
          url: credentials.payuUrlPayment ,
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization' : auth
          },
          strictSSL: false,
          body: data,
          json: true
        }, 

        function (err, response, body) {
          resolve(response.body);
        });
    })
   }
     /**
   *
   * Create payment on PayU 
   *
   * @param  {Object} params All order data (required)
   * @param  {Object} params All buyer data (required)
   * @param  {Object} params All payer data (required)
   *
   */
   service.create = function(order, buyer, payer, url){
    return new Promise((resolve, reject) => {
    var requireData = [
      order.referenceCode, 
      order.description, 
      order.value, 
      order.currency, 
      credentials.apiKey, 
      credentials.apiLogin, 
      credentials.merchantId, 
      credentials.accountId, 
      credentials.payuUrlPayment,
      url
    ];
    
    requireData.forEach(function(data){
      if(!data){
        reject({'require':'Payment data is require.'});
      }
    });

    var md5sum = crypto.createHash('md5');
    md5sum.update(credentials.apiKey+"~"+credentials.merchantId+"~"+ order.id +"~"+order.value+"~" + order.currency);

    var sig = md5sum.digest('hex');
    var data = {
      "language": "es",
      "command": "SUBMIT_TRANSACTION",
      "merchant": {
        "apiKey": credentials.apiKey,
        "apiLogin": credentials.apiLogin
      },
      "transaction": {
         "order": {
            "accountId": credentials.accountId,
            "referenceCode": order.referenceCode,
            "description": "payment test",
            "language": "es",
            "signature": sig,
            "notifyUrl": url,
            "additionalValues": {
              "TX_VALUE": {
                "value": order.value,
                "currency": order.currency
              }
            },
            "buyer": {
               "emailAddress": buyer.email
            }
         },
         "payer": {
            "fullName": payer.fullName,
            "emailAddress": payer.email,
            "contactPhone": payer.phone
         },
         "extraParameters": {
            "FINANCIAL_INSTITUTION_CODE": order.bank,
            "USER_TYPE": payer.userType,
            "PSE_REFERENCE2": payer.documentType,
            "PSE_REFERENCE3": payer.ID
         },
         "type": "AUTHORIZATION_AND_CAPTURE",
         "paymentMethod": "PSE",
         "paymentCountry": "CO"
      },
      "test": false
    }


    request({
        url: credentials.payuUrlPayment ,
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization' : auth
        },
        strictSSL: false,
        body: data,
        json: true
      }, 

      function (err, response, body) {
        resolve(response.body);
      });
    });
  };

  return service;
};