/**
 * Module dependencies.
 */
var request               = require('request');

/**
 * Expose
 */

module.exports = function (credentials) {

  var apiLogin              = credentials.apiLogin;
  var apiKey                = credentials.apiKey;
  auth                      = "Basic "+ new Buffer(apiLogin + ":" + apiKey).toString("base64");

  var service = {};

  /**
   * 
   * Create subscription on PayU 
   *
   * @param  {Object} params All customer data (required)
   * @param  {Object} params All plan data (required)
   * @param  {Object} params All card data (required)
   *
   */

  service.create = function(customer, plan, card){

    var requireData = [
      plan.planCode, 
      customer.id_payu, 
      card.token, 
      credentials.apiLogin, 
      credentials.apiKey, 
      credentials.payuUrlSubscription, 
      credentials.accountId
    ];

    requireData.forEach(function(data){
      if(!data){
        reject({'require':'Subscriptions data is require.'});
      }
    });

    return new Promise((resolve, reject) => {
      var data = {
        "quantity": "1",
        "installments": "1",
        "immediatePayment": true,
        "customer": {
          "id": customer.id_payu,
          "creditCards": [{
             "token": card.token
          }]
        },
        "plan": {
          "planCode": plan.planCode,
        },
        "extra2": plan.planCode
      };

      if (plan.notify_url)
        data.notifyUrl = plan.notify_url;

      if (customer['extra1'])
        data['extra1'] = customer['extra1'];

      request({
        url: credentials.payuUrlSubscription + 'subscriptions/',
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization' : auth
        },
        strictSSL: false,
        body: data,
        json: true
      }, 
      function (err, response, body) {   
        resolve(body);
      });
    });
  };

  /**
   * 
   * Find subscription on PayU 
   *
   * @param  {String} Id_payu of subscription (required)
   * 
   */

  service.find = function(subscriptionId){

    var requireData = [
      subscriptionId, 
      credentials.apiLogin, 
      credentials.apiKey, 
      credentials.payuUrlSubscription
    ];

    requireData.forEach(function(data){
      if(!data){
        reject({'require':'Subscription data is require.'});
      }
    });
    return new Promise((resolve, reject) => {
      request({
        url: credentials.payuUrlSubscription + 'subscriptions/' + subscriptionId,
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization' : auth
        },
        strictSSL: false,
        json: true
      }, 
      function (err, response, body) {   
        resolve(body);
      });
    });
  };

  /**
   * 
   * Update subscription on PayU 
   *
   * @param  {Object} params All customer data (required)
   * @param  {Object} params All plan data (required)
   * @param  {Object} params All card data (required)
   *
   */

  service.update = function(customer, plan, card, subscription){

    var requireData = [
      subscription.id_payu, 
      plan.planCode, 
      customer.id_payu, 
      card.token, 
      credentials.apiLogin, 
      credentials.apiKey, 
      credentials.payuUrlSubscription, 
      credentials.accountId
    ];

    requireData.forEach(function(data){
      if(!data){
        reject({'require':'Subscriptions data is require.'});
      }
    });

    return new Promise((resolve, reject) => {
      var data = {
        "quantity": "1",
        "installments": "1",
        "immediatePayment": true,
        "customer": {
          "id": customer.id_payu,
          "creditCards": [{
             "token": card.token
          }]
        },
        "plan": {
          "planCode": plan.planCode,
        }
      };
      request({
        url: credentials.payuUrlSubscription + 'subscriptions/'+subscription.id_payu,
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization' : auth
        },
        strictSSL: false,
        body: data,
        json: true
      },
      function (err, response, body) {   
        resolve(body);
      });
    });
  };

  /**
   * 
   * Delete subscription on PayU 
   *
   * @param  {String} Id_payu of subscription (required)
   * 
   */

  service.delete = function(subscriptionId){

    var requireData = [
      subscriptionId, 
      credentials.apiLogin, 
      credentials.apiKey, 
      credentials.payuUrlSubscription
    ];

    requireData.forEach(function(data){
      if(!data){
        reject({'require':'Subscription data is require.'});
      }
    });
    return new Promise((resolve, reject) => {
      request({
        url: credentials.payuUrlSubscription + 'subscriptions/'+ subscriptionId,
        method: 'DELETE',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization' : auth
        },
        strictSSL: false,
        json: true
      }, 
      function (err, response, body) {   
        resolve(body);
      });
    });
  };
  

  service.recurringBillBySubscriptionId = function(subscriptionId){
    if(!subscriptionId){
      reject({'require': 'Subscription Id is require.'});
    }
    return new Promise((resolve, reject) => {
      request({
        url: credentials.payuUrlSubscription + 'recurringBill?subscriptionId='+ subscriptionId,
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization' : auth
        },
        strictSSL: false,
        json: true
      }, 
      function (err, response, body) {   
        resolve(body);
      });
    });
  };

  return service;
};