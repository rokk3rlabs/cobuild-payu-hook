module.exports = function(credentials){

	var PayUHook = {};

	PayUHook.customer     = require('./lib/customer')(credentials);
	PayUHook.subscription = require('./lib/subscription')(credentials);
	PayUHook.payment      = require('./lib/payment')(credentials);
	PayUHook.plan         = require('./lib/plan')(credentials);
	PayUHook.bankTransfer = require('./lib/bankTransfer')(credentials);
	PayUHook.card		  = require('./lib/card')(credentials);

	return PayUHook;
}