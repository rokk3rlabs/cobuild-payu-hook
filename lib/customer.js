/**
 * Module dependencies.
 */

var request               = require('request');

/**
 * Expose
 */

module.exports = function(credentials){
  var apiLogin              = credentials.apiLogin;
  var apiKey                = credentials.apiKey;
  auth                      = "Basic "+ new Buffer(apiLogin + ":" + apiKey).toString("base64");

  var service = {};

  /**
   *
   * Create customer on PayU 
   *
   * @param  {Object} params All customer data (required)
   * 
   */

  service.create = function(customer){
    return new Promise((resolve, reject) => {

      var requireData = [
        customer.firstName, 
        customer.lastName, 
        customer.email, 
        credentials.apiLogin, 
        credentials.apiKey, 
        credentials.payuUrlSubscription
      ];

      requireData.forEach(function(data){
        if(!data){
          reject({'require':'Customer data is require.'});
        }
      });

      var data = {
        "fullName": customer.firstName +' '+ customer.lastName,
        "email": customer.email,
      };

      request({
        url: credentials.payuUrlSubscription + 'customers/',
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization' : auth
        },
        strictSSL: false,
        body: data,
        json: true
      }, 

      function (err, response, body) {      
        resolve(body);
      });
    });
  };

  /**
   * 
   * Delete customer on PayU 
   *
   * @param  {String} Id_payu of customer (required)
   * 
   */

  service.delete = function(customerId){
    return new Promise((resolve, reject) => {

      var requireData = [
        customerId, 
        credentials.apiLogin, 
        credentials.apiKey, 
        credentials.payuUrlSubscription
      ];

      requireData.forEach(function(data){
        if(!data){
          reject({'require':'Customer data is require.'})
        }
      });

      request({
        url: credentials.payuUrlSubscription + 'customers/' + customerId,
        method: 'DELETE',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization' : auth
        },
        strictSSL: false,
        json: true
      }, 

      function (err, response, body) {      
        resolve(body);
      });
    });
  };

  /**
   * 
   * Find customer on PayU 
   *
   * @param  {String} Id_payu of customer (required)
   * 
   */

  service.find = function(customerId){
    return new Promise((resolve, reject) => {

      var requireData = [
        customerId, 
        credentials.apiLogin, 
        credentials.apiKey, 
        credentials.payuUrlSubscription
      ];

      requireData.forEach(function(data){
        if(!data){
          reject({'require':'Customer data is require.'})
        }
      });

      request({
        url: credentials.payuUrlSubscription + 'customers/'+ customerId,
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization' : auth
        },
        strictSSL: false,
        json: true
      }, 
      function (err, response, body) {  
        resolve(body);
      });
    });
  };

  /**
   * 
   * Update customer on PayU 
   *
   * @param  {Object} params All customer data (required)
   * 
   */

  service.update = function(customer){
    return new Promise((resolve, reject) => {

      var requireData = [
        customer.firstName, 
        customer.lastName, 
        customer.email, 
        customer.id_payu, 
        credentials.apiLogin, 
        credentials.apiKey, 
        credentials.payuUrlSubscription
      ];

      requireData.forEach(function(data){
        if(!data){
          reject({'require':'Customer data is require.'});
        }
      });

      var data = {
        "fullName": customer.firstName +' '+ customer.lastName,
        "email": customer.email,
      };
      request({
        url: credentials.payuUrlSubscription + 'customers/'+ customer.id_payu,
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization' : auth
        },
        strictSSL: false,
        body: data,
        json: true
      },  
      function (err, response, body) {  
        resolve(body);
      });
    });
  };

  return service;
};