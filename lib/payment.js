/**
 * Module dependencies.
 */


var request               = require('request');
var crypto                = require('crypto');


/**
 * Expose
 */

module.exports = function (credentials) {

  var apiLogin              = credentials.apiLogin;
  var apiKey                = credentials.apiKey;
  auth                      = "Basic "+ new Buffer(apiLogin + ":" + apiKey).toString("base64");

  var service = {};  

  /**
   *
   * Create payment on PayU 
   *
   * @param  {Object} params All order data (required)
   * @param  {Object} params All customer data (required)
   * @param  {Object} params All shipping data (required)
   * @param  {Object} params All card data (required)
   *
   */

   service.cashPayment = function(order, buyer,url){
    return new Promise((resolve, reject) => {
      var requireData = [
        order.referenceCode, 
        order.description, 
        order.value, 
        order.currency, 
        credentials.apiKey, 
        credentials.apiLogin, 
        credentials.merchantId, 
        credentials.accountId, 
        credentials.payuUrlPayment,
        url
      ];
      
      requireData.forEach(function(data){
        if(!data){
          reject({'require':'Payment data is require.'});
        }
      });

      var md5sum = crypto.createHash('md5');
      md5sum.update(credentials.apiKey+"~"+credentials.merchantId+"~"+ order.id +"~"+order.value+"~" + order.currency);

      var sig = md5sum.digest('hex');
      var data = {
        "language": "es",
        "command": "SUBMIT_TRANSACTION",
        "merchant": {
          "apiKey": credentials.apiKey,
          "apiLogin": credentials.apiLogin
        },
         "transaction": {
            "order": {
              "notifyUrl": url,
              "accountId": credentials.accountId,
              "referenceCode": order.referenceCode,
               "description": order.description,
               "language": "es",
               "signature": sig,
               "additionalValues": {
                  "TX_VALUE": {
                     "value": order.value,
                     "currency": order.currency
                  }
                },
               "buyer": {
                  "fullName": buyer.fullName,
                  "emailAddress": buyer.email
               }
            },
            "type": "AUTHORIZATION_AND_CAPTURE",
            "paymentMethod":  order.paymentMethod,
            "expirationDate": order.expirationDate,
            "paymentCountry": "CO"
         },
        "test": false
      }


      request({
          url: credentials.payuUrlPayment ,
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization' : auth
          },
          strictSSL: false,
          body: data,
          json: true
        }, 

        function (err, response, body) {
          resolve(response.body);
        });
      });
  };

  service.create = function(order, buyer, shipping, card, extraParameters, url){
    return new Promise((resolve, reject) => {
      var requireData = [
        order.referenceCode, 
        order.description, 
        order.value, 
        order.currency, 
        buyer.name, 
        buyer.email, 
        buyer.phone, 
        shipping.line1,
        card.token, 
        card.method, 
        shipping.city, 
        credentials.apiKey, 
        credentials.apiLogin, 
        credentials.merchantId, 
        credentials.accountId, 
        credentials.payuUrlPayment,
        url
      ];
      
      requireData.forEach(function(data){
        if(!data){
          reject({'require':'Payment data is require.'});
        }
      });

      var md5sum = crypto.createHash('md5');
      md5sum.update(credentials.apiKey+"~"+credentials.merchantId+"~" + order.id + "~" + order.amount + "~" + order.currency);
      var sig = md5sum.digest('hex');
      var data = {
        "language": "es",
        "command": "SUBMIT_TRANSACTION",
        "merchant": {
          "apiKey": credentials.apiKey,
          "apiLogin": credentials.apiLogin
        },
        "transaction": {
          "order": {
            "notifyUrl": url,
            "accountId": credentials.accountId,
            "referenceCode": order.referenceCode,
            "description": order.description,
            "language": order.language,
            "signature": sig,
            "additionalValues": {
              "TX_VALUE": {
                "value": order.value,
                "currency": order.currency
              }
            },
            "buyer": {
              "fullName": buyer.name,
              "emailAddress": buyer.email,
              "contactPhone": buyer.phone,
              "shippingAddress": {
                "street1": shipping.line1,
                "street2": shipping.line2 || '',
                "city": shipping.city,
                "state": shipping.state || '',
                "country": shipping.country,
                "postalCode": shipping.zipcode || '',
                "phone": buyer.phone || ''
              }
            },
            "shippingAddress": {
              "street1": shipping.line1 || '',
              "street2": shipping.line2 || '',
              "city": shipping.city || '',
              "state": shipping.state || '',
              "country": shipping.country,
              "postalCode": shipping.zipcode || '',
              "phone": buyer.phone || ''
            }
          },
          "payer": {
            "fullName": buyer.name,
            "emailAddress": buyer.email,
            "contactPhone": buyer.phone,
            "billingAddress": {
              "street1": shipping.line1,
              "street2": shipping.line2 || '',
              "city": shipping.city,
              "state": shipping.state || '',
              "country": shipping.country,
              "postalCode": shipping.zipcode || '',
              "phone": buyer.phone || ''
            }
          },
          "extraParameters": {
            "INSTALLMENTS_NUMBER": extraParameters.installmentsNumber || 1
          },
          "creditCardTokenId": card.token,
          "type": "AUTHORIZATION_AND_CAPTURE",
          "paymentMethod": card.method,
          "paymentCountry": "CO"
        },
        "test": false 
      };

      request({
        url: credentials.payuUrlPayment,
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization' : auth
        },
        strictSSL: false,
        body: data,
        json: true
      }, 

      function (err, response, body) {
        resolve(response.body);
      });
    });
  };

  return service;
};