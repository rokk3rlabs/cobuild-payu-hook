/**
 * Module dependencies.
 */

var request               = require('request');

/**
 * Expose
 */

module.exports = function (credentials) {

  var apiLogin              = credentials.apiLogin;
  var apiKey                = credentials.apiKey;
  auth                      = "Basic "+ new Buffer(apiLogin + ":" + apiKey).toString("base64");

  var service = {};

  /**
   * 
   * Create plan on PayU 
   *
   * @param  {Object} params All plan data (required)
   * 
   */

  service.create = function(plan){

    return new Promise((resolve, reject) => {
      var requireData = [
        plan.planCode, 
        plan.description, 
        plan.intervalCount, 
        plan.interval, 
        plan.maxPaymentsAllowed, 
        plan.paymentAttemptsDelay, 
        plan.value, plan.currency, 
        credentials.apiLogin, 
        credentials.apiKey, 
        credentials.payuUrlSubscription, 
        credentials.accountId
      ];

      requireData.forEach(function(data){
        if(!data){
          reject({'require':'Plan data is require.'});
        }
      });

      var data = {
        "planCode": plan.planCode,
        "description": plan.description,
        "accountId": credentials.accountId,
        "intervalCount": plan.intervalCount,
        "interval": plan.interval,
        "maxPaymentsAllowed": plan.maxPaymentsAllowed,
        "paymentAttemptsDelay": plan.paymentAttemptsDelay, 
        "additionalValues":[{
          "name": "PLAN_VALUE",
          "value": plan.value,
          "currency": plan.currency
        }]
      };

      request({
        url: credentials.payuUrlSubscription + 'plans/',
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization' : auth
        },
        strictSSL: false,
        body: data,
        json: true
      }, 

      function (err, response, body) {      
        resolve(body);
      });
    });

  };

  /**
   * 
   * Find plan on PayU 
   *
   * @param  {String} planCode  (required)
   * 
   */

  service.find = function(planId){
    return new Promise((resolve, reject) => {

      var requireData = [
        planId, 
        credentials.apiLogin, 
        credentials.apiKey, 
        credentials.payuUrlSubscription
      ];

      requireData.forEach(function(data){
        if(!data){
          reject({'require':'Plan data is require.'});
        }
      });

      request({
        url: credentials.payuUrlSubscription + 'plans/'+planId,
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization' : auth
        },
        strictSSL: false,
        json: true
      },function (err, response, body) {  
        resolve(body);
      });

    });
  };

  /**
   * 
   * Update plan on PayU 
   *
   * @param  {Object} params All plan data (required)
   * 
   */
  
  service.update = function(plan){
    return new Promise((resolve, reject) => {
      var requireData = [
        plan.planCode, 
        plan.description,  
        plan.paymentAttemptsDelay, 
        plan.value, 
        plan.currency, 
        credentials.apiLogin, 
        credentials.apiKey, 
        credentials.payuUrlSubscription
      ];

      requireData.forEach(function(data){
        if(!data){
          reject({'require':'Plan data is require.'});
        }
      });

      var data = {
        "planCode": plan.planCode,
        "description": plan.description,
        "paymentAttemptsDelay": plan.paymentAttemptsDelay, 
        "additionalValues":[{
          "name": "PLAN_VALUE",
          "value": plan.value,
          "currency": plan.currency
        }]
      };
      request({
        url: credentials.payuUrlSubscription + 'plans/'+plan.planCode,
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization' : auth
        },
        strictSSL: false,
        body: data,
        json: true
      },
      function (err, response, body) {     
        resolve(body);
      });
    });
  };

  /**
   * 
   * Delete plan on PayU 
   *
   * @param  {String} planCode (required)
   * 
   */

  service.delete = function(planId){

    return new Promise((resolve, reject) => {

      var requireData = [
        planId, 
        credentials.apiLogin, 
        credentials.apiKey, 
        credentials.payuUrlSubscription
      ];

      requireData.forEach(function(data){
        if(!data){
          reject({'require':'Plan data is require.'})
        }
      });

      request({
        url: credentials.payuUrlSubscription + 'plans/'+planId,
        method: 'DELETE',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8',
          'Authorization' : auth
        },
        strictSSL: false,
        json: true
      },
      function (err, response, body) {
        resolve(body);
      });
    });
  };
  return service;
};