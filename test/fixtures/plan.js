module.exports = {
  "planCode": "sample-plan-code-001",
  "description": "Sample Plan 001",
  "interval": "MONTH",
  "intervalCount": "1",
  "maxPaymentsAllowed": "12",
  "paymentAttemptsDelay": "1",
  "name": "PLAN_VALUE",
  "value": "20000",
  "currency": "COP"
}