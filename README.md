# Cobuild PayU Hook
This is a module that allows to add a subscription with a customer, plan and card in [PayU](https://corporate.payu.com/)

## Auth 
The auth always auth should be in base64 and contain apiLogin : apiKey

```
auth = "Basic "+ new Buffer(<your apiLogin> + ":" + <your apiKey>).toString("base64");
```

## Payment

```
var md5sum = crypto.createHash('md5');
md5sum.update(config.payu.payuApiKey+"~"+config.payu.merchantId+"~" + order.id + "~" + order.amount + "~" + order.currency);
var sig = md5sum.digest('hex');
var data = {
  "command": "SUBMIT_TRANSACTION",
  "merchant": {
    "apiKey": <your apiKey>,
    "apiLogin": <your apiLogin> 
  },
  "transaction": {
    "order": {
      "accountId": <your accountId>,
      "referenceCode": <order id>,
      "description": <order description>,
      "signature": sig,
      "additionalValues": {
        "TX_VALUE": {
          "value": <order amount>,
          "currency": <order currency>
        }
      },
      "buyer": {
        "fullName": <buyer name>,
        "emailAddress": <buyer email>,
        "contactPhone": <buyer phone>,
        "shippingAddress": {
          "street1": <shipping line1>,
          "street2": <shipping line2>,
          "city": <shipping city>,
          "state": <shipping state>,
          "country": "CO",
          "postalCode": <shipping zipcode>,
          "phone": <buyer phone>
        }
      },
      "shippingAddress": {
        "street1": <shipping line1>,
        "street2": <shipping line2>,
        "city": <shipping city>,
        "state": <shipping state>,
        "country": "CO",
        "postalCode": <shipping zipcode>,
        "phone": <buyer phone>
      }
    },
    "payer": {
      "fullName": <buyer name>,
      "emailAddress": <buyer email>,
      "contactPhone": <buyer phone>,
      "billingAddress": {
        "street1": <shipping line1>,
        "street2": <shipping line2>,
        "city": <shipping city>,
        "state": <shipping state>,
        "country": "CO",
        "postalCode": <shipping zipcode>,
        "phone": <buyer phone>
      }
    },
    "creditCardTokenId": <card token>,
    "type": "AUTHORIZATION_AND_CAPTURE",
    "paymentMethod": <card method>,
    "paymentCountry": "CO"
  },
  "test": false 
};
request({
  url: config.payu.payuUrlPayment,
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization' : auth
  },
  strictSSL: false,
  body: data,
  json: true
}
```

## Subscription


### Create

#### Create customer

```
var data = {
  "fullName": '<your full name>',
  "email": '<your email>',
};
request({
  url: 'https://sandbox.api.payulatam.com/payments-api/rest/v4.9/customers/',
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization' : auth
  },
  strictSSL: false,
  body: data,
  json: true
});
```

#### Create plan

```
var data = {
  "planCode": <plan code>,
  "description": <description>,
  "accountId": <your accountId>,
  "intervalCount": <interval count>
  "interval": <interval>,
  "maxPaymentsAllowed": <max payments allowed>,
  "paymentAttemptsDelay": <payment attempts delay>,      
  "additionalValues":[{
    "name": <name>,
    "value": <value>,
    "currency": <currency>
  }]       
};

request({
  url: 'https://sandbox.api.payulatam.com/payments-api/rest/v4.9/plans/',
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization' : auth
  },
  strictSSL: false,
  body: data,
  json: true
});
```

#### Create subscription

```
var data = {
  "quantity": <quantity>,
  "installments": <installments>,
  "immediatePayment": <true or false>,
  "customer": {
    "id": <id>,
    "creditCards": [{
       "token": <token>
    }]
  },
  "plan": {
    "planCode": <plan code>,
  },
  "deliveryAddress": {
    "line1": <address line1>,
    "line2": <address line2>,
    "line3": <address line3>,
    "city": <city>,
    "country": <country>,
    "phone": <phone>
  } 
};

request({
  url: 'https://sandbox.api.payulatam.com/payments-api/rest/v4.9/subscriptions/',
  method: 'POST',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization' : auth
  },
  strictSSL: false,
  body: data,
  json: true
});
```

### Update

#### Update customer

```
var data = {
  "id_payu": '<id generated to PayU>',
  "fullName": '<your full name>',
  "email": '<your email>'
};
request({
  url: 'https://sandbox.api.payulatam.com/payments-api/rest/v4.9/customers/'+ data.id_payu,
  method: 'PUT',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization' : auth
  },
  strictSSL: false,
  body: data,
  json: true
})
```

#### Update plan

```
var data = {
  "id_payu": '<id generated to PayU>',
  "planCode": <plan code>,
  "description": <description>,
  "accountId": <your accountId>,
  "intervalCount": <interval count>
  "interval": <interval>,
  "maxPaymentsAllowed": <max payments allowed>,
  "paymentAttemptsDelay": <payment attempts delay>,      
  "additionalValues":[{
    "name": <name>,
    "value": <value>,
    "currency": <currency>
  }]       
};

request({
  url: 'https://sandbox.api.payulatam.com/payments-api/rest/v4.9/plans/'+data.planCode,
  method: 'PUT',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization' : auth
  },
  strictSSL: false,
  body: data,
  json: true
})
```

#### Update subscription

```
var data = {
  "id_payu": '<id generated to PayU>',
  "quantity": <quantity>,
  "installments": <installments>,
  "immediatePayment": <true or false>,
  "customer": {
    "id": <id>,
    "creditCards": [{
       "token": <token>
    }]
  },
  "plan": {
    "planCode": <plan code>,
  },
  "deliveryAddress": {
    "line1": <address line1>,
    "line2": <address line2>,
    "line3": <address line3>,
    "city": <city>,
    "country": <country>,
    "phone": <phone>
  } 
};

request({
  url: 'https://sandbox.api.payulatam.com/payments-api/rest/v4.9/subscriptions/'+subscription.id_payu,
  method: 'PUT',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization' : auth
  },
  strictSSL: false,
  body: subscription,
  json: true
})
```

### Find

#### Find customer

```
var data = {
  "id_payu": '<id generated to PayU>',
};

request({
  url: 'https://sandbox.api.payulatam.com/payments-api/rest/v4.9/customers/'+ data.id_payu,
  method: 'GET',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization' : auth
  },
  strictSSL: false,
  json: true
})
```

#### Find plan

```
var data = {
  "id_payu": '<id generated to PayU>',
};

request({
  url: 'https://sandbox.api.payulatam.com/payments-api/rest/v4.9/plans/'+data.id_payu,
  method: 'GET',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization' : auth
  },
  strictSSL: false,
  json: true
})
```
#### Find subscription

```
var data = {
  "id_payu": '<id generated to PayU>',
};

request({
  url: 'https://sandbox.api.payulatam.com/payments-api/rest/v4.9/subscriptions/'+data.id_payu,
  method: 'GET',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization' : auth
  },
  strictSSL: false,
  json: true
})
```

