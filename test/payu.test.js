var should = require('should');

describe('PayU Hook', function(){
  var fixtures = {};

  before(function (done) {
      loadFixtures();
      done();
  });

  it('Should create customer ', function (done) {
      var payUHook = require(process.cwd()+'/index.js')(fixtures.credentialsPayU);
      payUHook.customer.create(fixtures.customer)
        .then(function(customer){
          customer.should.have.property('id');
          done()
        })
        .catch(function(err){
          console.log(err.require);
          done()
        })
  });

  it('Should find customer ', function (done) {
      var payUHook = require(process.cwd()+'/index.js')(fixtures.credentialsPayU);
      payUHook.customer.find(fixtures.customer.id_payu)
        .then(function(customer){
          customer.should.not.have.property('type')
          done()
        })
        .catch(function(err){
          if(err.require){
            console.log(err.require)
            done()
          }else{
            console.log("Customer not found")
            done()
          }
        })
  });

  it('Should update customer ', function (done) {
      var payUHook = require(process.cwd()+'/index.js')(fixtures.credentialsPayU);
      payUHook.customer.update(fixtures.customer)
        .then(function(customer){
          customer.should.not.have.property('type');
          done()
        })
        .catch(function(err){
          if(err.require){
            console.log(err.require)
            done()
          }else{
            console.log("Customer not found")
            done()
          }
        })
  });

  it('Should delete customer ', function (done) {
      var payUHook = require(process.cwd()+'/index.js')(fixtures.credentialsPayU);
      payUHook.customer.delete(fixtures.customer.id_payu)
        .then(function(customer){
          customer.should.not.have.property('type');
          done()
        })
        .catch(function(err){
          if(err.require){
            console.log(err.require)
            done()
          }else{
            console.log("Customer not found")
            done()
          }
        })
  });

  it('Should create plan ', function (done) {
      var payUHook = require(process.cwd()+'/index.js')(fixtures.credentialsPayU);
      payUHook.plan.create(fixtures.plan)
        .then(function(plan){
          plan.should.have.property('id');
          done()
        })
        .catch(function(err){
          if(err.require){
            console.log(err.require);
            done()
          }else{
            console.log("Plan already exists")
            done()
          }
        })
  });

  it('Should find plan ', function (done) {
    var payUHook = require(process.cwd()+'/index.js')(fixtures.credentialsPayU);
    payUHook.plan.find(fixtures.plan.planCode)
      .then(function(plan){
        plan.should.not.have.property('type')
        done()
      })
      .catch(function(err){
        if(err.require){
          console.log(err.require)
          done()
        }else{
          console.log("Plan not found")
          done()
        }
      })
  });

  it('Should update plan ', function (done) {
      var payUHook = require(process.cwd()+'/index.js')(fixtures.credentialsPayU);
      payUHook.plan.update(fixtures.plan)
        .then(function(plan){
          plan.should.not.have.property('type');
          done()
        })
        .catch(function(err){
          if(err.require){
            console.log(err.require)
            done()
          }else{
            console.log("Plan not found")
            done()
          }
        })
  });

  it('Should delete plan ', function (done) {
    var payUHook = require(process.cwd()+'/index.js')(fixtures.credentialsPayU);
    payUHook.plan.delete(fixtures.plan.planCode)
      .then(function(plan){
        plan.should.not.have.property('type');
        done()
      })
      .catch(function(err){
        if(err.require){
          console.log(err.require)
          done()
        }else{
          console.log("Plan not found")
          done()
        }
      })
  });

  it('Should create subscription ', function (done) {
    var payUHook = require(process.cwd()+'/index.js')(fixtures.credentialsPayU);
    payUHook.subscription.create(fixtures.customer, fixtures.plan, fixtures.card)
      .then(function(subscription){
        subscription.should.have.property('id');
        done()
      })
      .catch(function(err){
        if(err.require){
          console.log(err.require)
          done()
        }else{
          console.log("Error")
          done()
        }
      })
  });

  it('Should find subscription ', function (done) {
    var payUHook = require(process.cwd()+'/index.js')(fixtures.credentialsPayU);
    payUHook.subscription.find(fixtures.subscription.id_payu)
      .then(function(subscription){
        subscription.should.not.have.property('type')
        done()
      })
      .catch(function(err){
        if(err.require){
          console.log(err.require)
          done()
        }else{
          console.log("Subscription not found")
          done()
        }
      })
  });

  it('Should update subscription ', function (done) {
    var payUHook = require(process.cwd()+'/index.js')(fixtures.credentialsPayU);
    payUHook.subscription.update(fixtures.customer, fixtures.plan, fixtures.card, fixtures.subscription)
      .then(function(subscription){
        subscription.should.not.have.property('type');
        done()
      })
      .catch(function(err){
        if(err.require){
          console.log(err.require)
          done()
        }else{
          console.log("Subscription not found")
          done()
        }
      })
  });

  it('Should delete subscription ', function (done) {
    var payUHook = require(process.cwd()+'/index.js')(fixtures.credentialsPayU);
    payUHook.subscription.delete(fixtures.subscription.id_payu)
      .then(function(subscription){
        subscription.should.not.have.property('type');
        done()
      })
      .catch(function(err){
        if(err.require){
          console.log(err.require)
          done()
        }else{
          console.log("Subscription not found")
          done()
        }
      })
  });

  it('Should create payment ', function (done) {
    var payUHook = require(process.cwd()+'/index.js')(fixtures.credentialsPayU);
    payUHook.payment.create(fixtures.order, fixtures.customer, fixtures.shipping, fixtures.card)
      .then(function(payment){
        payment.should.have.property('id');
        done()
      })
      .catch(function(err){
        console.log("Error")
        done()
      })
  });

  function loadFixtures(argument) {

    var Cobuild = require('cobuild2');
    var path = require('path');

    Cobuild.Utils.Files.listFilesInFolder(__dirname + '/fixtures')
    .forEach(function (file) {
        var completePath =  path.join(__dirname , 'fixtures',  file)
        fixtures[file.slice(0, -3)] = require(completePath);
        
    });
  }
});
